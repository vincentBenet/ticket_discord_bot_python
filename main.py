import discord
from bot_commands import *
from settings import *

async def loop(message):
    id = client.get_guild(ID)
    global a
    a = message
    print(message)
    if message.content[0] != "!":
        return
    await main(message)
async def main(message):
    try:
        await message.delete()
    except:
        pass
    command = message.content.split(" ")[0]
    if not(command in commands):
        try:
            exec("await {0}".format(message.content[1:]))
        except:
            await typo(message)
    else:
        input = message.content[1:]
        list = input.split(" ")
        args = []
        for i in range(1,len(list)):
            args.append(list[i])
        await commands[command]['function'](message,args)
async def typo(message):
    txt = "{0}\n{1} is not valid!\nCommands:\n".format(message.author.mention,message.content)
    for command in commands:
        explaination = commands[command]["explain"]
        txt += "\t{0} ( {1} )\n".format(command,explaination)
    await message.author.send(txt)

if __name__ == "__main__":
    client = discord.Client()
    ID = client.get_guild(discord_id_server)
    @client.event
    async def on_message(message):
        await loop(message)
    client.run(discord_tocken_bot)