from bot_functions import *
commands={
    '!test': {
        'function': test,
        'explain': "Test if bot is working"
    },
    '!create': {
        'function': create,
        'explain': "Create a new channel: !create channel_name"
    },
    '!remove': {
        'function': remove,
        'explain': "Close current channel: !remove"
    }
}