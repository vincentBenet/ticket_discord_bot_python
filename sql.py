import mysql.connector
import time
from time import time as T
from time import sleep

def log(message):
    global t0
    try:
        t0
    except:
        t0 = time.time()
    print('sql',time.time()-t0,message)

def open(config):
    log('open'+' - '+str(config['database'])+' - '+str(config['host']))
    while True:
        try:
            connection = mysql.connector.connect(**config)
            break
        except mysql.connector.errors.DatabaseError:
            log('no connection'+' - '+str(config['database'])+' - '+str(config['host']))
    log('connected'+' - '+str(config['database'])+' - '+str(config['host']))
    return connection

def execute(connection,querry):
    log('execute'+'\n'+100*'_'+'\n'+str(querry)+'\n'+100*'_')
    cursor = connection.cursor()
    cursor.execute(querry)
    return cursor.fetchall()
    
def commit(connection):
    log('commit')
    return connection.commit()

def close(connection):
    log('close')
    return connection.close()

def sql(querry,config):
    global connection
    try:
        connection
    except:
        connection = open(config)
    result = execute(connection,querry)
    log('result'+'\n'+100*'-'+'\n'+str(result)+'\n'+100*'-')
    if querry.split(" ")[0].upper() in ["SELECT","SHOW"]:
        return result
    commit(connection)