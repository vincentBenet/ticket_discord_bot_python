import sql

def create_table(table,columns):
    txt = ""
    for column in columns:
        txt+="{0} {1},\n"
    txt = txt[:-1] 
    sql.sql("""
        CREATE TABLE '{0}' IF NOT EXIST(
            {1}
        );
    """.format(table,txt))